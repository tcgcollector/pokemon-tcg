using EgonOlieux.Common.Extensions;

namespace TcgCollector.PokemonTcg;

public sealed class CardNumberPart(string cardNumberPart) : IComparable<CardNumberPart>
{
    private readonly Lazy<ParsedCardNumberPart> lazyParsedCardNumberPart = new(() =>
        ParseCardNumberPart(cardNumberPart)
    );

    public string Value { get; } = cardNumberPart;

    public int? IntValue => this.lazyParsedCardNumberPart.Value.IntValue;

    public int Trailing0Count => this.lazyParsedCardNumberPart.Value.Trailing0Count;

    public string? Prefix => this.lazyParsedCardNumberPart.Value.Prefix;

    public string? Suffix => this.lazyParsedCardNumberPart.Value.Suffix;

    public int CompareTo(CardNumberPart? otherCardNumberPart)
    {
        if (otherCardNumberPart == null)
        {
            return 1;
        }

        var intValue = this.IntValue ?? int.MinValue;
        var otherIntValue = otherCardNumberPart.IntValue ?? int.MinValue;

        return intValue.CompareTo(otherIntValue);
    }

    private static ParsedCardNumberPart ParseCardNumberPart(string cardNumberPart)
    {
        int? intValue = null;
        var intValueString = string.Empty;
        var trailing0Count = 0;
        var prefix = string.Empty;
        var suffix = string.Empty;
        var isPreviousCharNumeric = false;
        var alternatingIntStringCount = 0;

        // Loop trough each character of the card number part and build the int value string, prefix and suffix.
        for (var i = 0; i < cardNumberPart.Length; i++)
        {
            var isCharNumeric = char.IsNumber(cardNumberPart[i]);

            if (i != 0 && isCharNumeric != isPreviousCharNumeric)
            {
                alternatingIntStringCount++;
            }

            if (isCharNumeric && suffix.Length == 0)
            {
                intValueString += cardNumberPart[i];
            }
            else if (!isCharNumeric && alternatingIntStringCount == 0)
            {
                prefix += cardNumberPart[i];
            }
            else if (alternatingIntStringCount != 0)
            {
                suffix += cardNumberPart[i];
            }

            isPreviousCharNumeric = isCharNumeric;
        }

        if (!string.IsNullOrEmpty(intValueString))
        {
            intValue = int.Parse(intValueString);
            trailing0Count = intValueString.Length - intValueString.TrimStart('0').Length;
        }

        return new ParsedCardNumberPart
        {
            IntValue = intValue,
            Trailing0Count = trailing0Count,
            Prefix = (!string.IsNullOrEmpty(prefix) && prefix.Length < cardNumberPart.Length)
                ? prefix
                : null,
            Suffix = suffix.ToNullIfEmpty(),
        };
    }

    private sealed record ParsedCardNumberPart
    {
        public int? IntValue { get; init; }

        public required int Trailing0Count { get; init; }

        public string? Prefix { get; init; }

        public string? Suffix { get; init; }
    }
}
