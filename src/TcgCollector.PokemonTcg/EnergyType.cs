using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace TcgCollector.PokemonTcg;

[JsonConverter(typeof(StringEnumConverter))]
public enum EnergyType
{
    Grass,
    Fire,
    Water,
    Lightning,
    Psychic,
    Fighting,
    Darkness,
    Metal,
    Fairy,
    Dragon,
    Colorless,
}
