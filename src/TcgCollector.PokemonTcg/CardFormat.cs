using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace TcgCollector.PokemonTcg;

[JsonConverter(typeof(StringEnumConverter))]
public enum CardFormat
{
    Unlimited,
    Expanded,
    Standard,
}
