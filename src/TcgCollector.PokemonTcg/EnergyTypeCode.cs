using System.Collections.Immutable;

namespace TcgCollector.PokemonTcg;

public static class EnergyTypeCode
{
    private static readonly ImmutableSortedDictionary<char, EnergyType> CodeToEnergyTypeMap =
        new Dictionary<char, EnergyType>
    {
        { 'C', EnergyType.Colorless },
        { 'D', EnergyType.Darkness },
        { 'F', EnergyType.Fighting },
        { 'G', EnergyType.Grass },
        { 'L', EnergyType.Lightning },
        { 'M', EnergyType.Metal },
        { 'N', EnergyType.Dragon },
        { 'P', EnergyType.Psychic },
        { 'R', EnergyType.Fire },
        { 'W', EnergyType.Water },
        { 'Y', EnergyType.Fairy },
    }.ToImmutableSortedDictionary();

    private static readonly ImmutableSortedDictionary<EnergyType, char> EnergyTypeToCodeMap =
        CodeToEnergyTypeMap.ToImmutableSortedDictionary(
            keyValuePair => keyValuePair.Value,
            keyValuePair => keyValuePair.Key
        );

    public static bool CodeExists(char code) => CodeToEnergyTypeMap.ContainsKey(code);

    public static IEnumerable<char> GetCodes() => CodeToEnergyTypeMap.Keys;

    public static char GetCodeByEnergyType(EnergyType energyType) => EnergyTypeToCodeMap[energyType];

    public static bool TryGetCodeByEnergyType(EnergyType energyType, out char code) =>
        EnergyTypeToCodeMap.TryGetValue(energyType, out code);

    public static EnergyType GetEnergyTypeByCode(char code) => CodeToEnergyTypeMap[code];

    public static bool TryGetEnergyTypeByCode(char code, out EnergyType energyType) =>
        CodeToEnergyTypeMap.TryGetValue(code, out energyType);
}
