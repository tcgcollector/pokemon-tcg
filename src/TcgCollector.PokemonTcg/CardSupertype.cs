using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.Runtime.Serialization;

namespace TcgCollector.PokemonTcg;

[JsonConverter(typeof(StringEnumConverter))]
public enum CardSupertype
{
    [EnumMember(Value = "Pokémon")]
    Pokemon,

    Trainer,

    Energy,
}
