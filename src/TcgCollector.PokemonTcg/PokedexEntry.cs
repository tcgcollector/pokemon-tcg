namespace TcgCollector.PokemonTcg;

public sealed record PokedexEntry : IComparable<PokedexEntry>
{
    public required int Number { get; init; }

    public required string PokemonName { get; init; }

    public int CompareTo(PokedexEntry? otherPokedexEntry) => (otherPokedexEntry != null)
        ? this.Number.CompareTo(otherPokedexEntry.Number)
        : 1;
}
