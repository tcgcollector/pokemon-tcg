using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.Runtime.Serialization;

namespace TcgCollector.PokemonTcg;

[JsonConverter(typeof(StringEnumConverter))]
public enum CardLanguage
{
    [EnumMember(Value = "Chinese (Simp.)")]
    ChineseSimplified,

    [EnumMember(Value = "Chinese (Trad.)")]
    ChineseTraditional,

    Dutch,

    English,

    French,

    German,

    Indonesian,

    Italian,

    Japanese,

    Korean,

    Polish,

    Portuguese,

    Russian,

    Spanish,

    Thai,
}
