using System.Collections.Immutable;

namespace TcgCollector.PokemonTcg;

public static class CardLanguageCode
{
    private static readonly ImmutableSortedDictionary<string, CardLanguage> CodeToCardLanguageMap =
        new Dictionary<string, CardLanguage>
    {
        { "DE", CardLanguage.German },
        { "EN", CardLanguage.English },
        { "ES", CardLanguage.Spanish },
        { "FR", CardLanguage.French },
        { "ID", CardLanguage.Indonesian },
        { "IT", CardLanguage.Italian },
        { "JP", CardLanguage.Japanese },
        { "KO", CardLanguage.Korean },
        { "NL", CardLanguage.Dutch },
        { "PL", CardLanguage.Polish },
        { "PT", CardLanguage.Portuguese },
        { "RU", CardLanguage.Russian },
        { "TH", CardLanguage.Thai },
        { "ZH-S", CardLanguage.ChineseSimplified },
        { "ZH-T", CardLanguage.ChineseTraditional },
    }.ToImmutableSortedDictionary();

    private static readonly ImmutableSortedDictionary<CardLanguage, string> CardLanguageToCodeMap =
        CodeToCardLanguageMap.ToImmutableSortedDictionary(
            keyValuePair => keyValuePair.Value,
            keyValuePair => keyValuePair.Key
        );

    public static bool CodeExists(string code) => CodeToCardLanguageMap.ContainsKey(code);

    public static IEnumerable<string> GetCodes() => CodeToCardLanguageMap.Keys;

    public static string GetCodeByCardLanguage(CardLanguage cardLanguage) =>
        CardLanguageToCodeMap[cardLanguage];

    public static bool TryGetCodeByCardLanguage(CardLanguage cardLanguage, out string? code) =>
        CardLanguageToCodeMap.TryGetValue(cardLanguage, out code);

    public static CardLanguage GetCardLanguageByCode(string code) => CodeToCardLanguageMap[code];

    public static bool TryGetCardLanguageByCode(string code, out CardLanguage cardLanguage) =>
        CodeToCardLanguageMap.TryGetValue(code, out cardLanguage);
}
