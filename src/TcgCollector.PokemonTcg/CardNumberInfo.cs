namespace TcgCollector.PokemonTcg;

public sealed class CardNumberInfo
{
    public CardNumberInfo(string cardNumber)
    {
        var cardNumberParts = cardNumber.Split('/');

        if (!AreCardNumberPartsValid(cardNumberParts))
        {
            throw new FormatException(
                $"Card number \"{cardNumber}\" must contain at most 1 \"/\" character; " +
                $"{cardNumberParts.Length - 1} were found."
            );
        }

        this.CardNumber = cardNumber;
        this.LeftPart = new CardNumberPart(cardNumberParts[0]);

        if (cardNumberParts.Length == 2)
        {
            this.RightPart = new CardNumberPart(cardNumberParts[1]);
        }
    }

    public string CardNumber { get; }

    public CardNumberPart LeftPart { get; }

    public CardNumberPart? RightPart { get; }

    public bool IsLeftPartGreater =>
        this.LeftPart.IntValue != null &&
        this.RightPart?.IntValue != null &&
        this.LeftPart.IntValue > this.RightPart.IntValue;

    public static bool IsCardNumberValid(string cardNumber) => AreCardNumberPartsValid(cardNumber.Split('/'));

    public bool IsCompatible(CardNumberInfo cardNumberInfo) =>
        this.RightPart?.Value == cardNumberInfo.RightPart?.Value &&
        this.LeftPart.Prefix == cardNumberInfo.LeftPart.Prefix &&
        this.LeftPart.Suffix == cardNumberInfo.LeftPart.Suffix;

    private static bool AreCardNumberPartsValid(string[] cardNumberParts) => cardNumberParts.Length < 3;
}
