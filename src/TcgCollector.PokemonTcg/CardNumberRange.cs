using EgonOlieux.Common.Extensions;
using System.Diagnostics.CodeAnalysis;

namespace TcgCollector.PokemonTcg;

public sealed class CardNumberRange
{
    public const string DefaultRangeSeparator = " – ";

    private List<string>? lazyCardNumbers;

    public CardNumberRange(string firstCardNumber, string lastCardNumber)
        : this(new CardNumberInfo(firstCardNumber), new CardNumberInfo(lastCardNumber))
    {
    }

    public CardNumberRange(CardNumberInfo firstCardNumberInfo, CardNumberInfo lastCardNumberInfo)
    {
        var areCardNumbersEqual = firstCardNumberInfo.CardNumber == lastCardNumberInfo.CardNumber;

        if (!areCardNumbersEqual)
        {
            if (!firstCardNumberInfo.IsCompatible(lastCardNumberInfo))
            {
                throw new ArgumentException(
                    "The card numbers in range \"" +
                    firstCardNumberInfo.CardNumber +
                    DefaultRangeSeparator +
                    lastCardNumberInfo.CardNumber +
                    "\" aren't compatible."
                );
            }

            if (!IsExpandable(firstCardNumberInfo, lastCardNumberInfo))
            {
                throw new ArgumentException(
                    "Card number range \"" +
                    firstCardNumberInfo.CardNumber +
                    DefaultRangeSeparator +
                    lastCardNumberInfo.CardNumber +
                    "\" isn't expandable."
                );
            }
        }

        this.FirstCardNumberInfo = firstCardNumberInfo;
        this.LastCardNumberInfo = lastCardNumberInfo;
        this.AreCardNumbersEqual = areCardNumbersEqual;
    }

    public CardNumberRange(string cardNumber)
        : this(new CardNumberInfo(cardNumber))
    {
    }

    public CardNumberRange(CardNumberInfo cardNumberInfo)
        : this(cardNumberInfo, cardNumberInfo)
    {
    }

    public string FirstCardNumber => this.FirstCardNumberInfo.CardNumber;

    public CardNumberInfo FirstCardNumberInfo { get; }

    public string LastCardNumber => this.LastCardNumberInfo.CardNumber;

    public CardNumberInfo LastCardNumberInfo { get; }

    public bool AreCardNumbersEqual { get; }

    /// <summary>
    /// <see cref="Parse(string, string)"/>
    /// </summary>
    public static CardNumberRange Parse(string cardNumberRangeString, char rangeSeparator) =>
        Parse(cardNumberRangeString, rangeSeparator.ToString());

    /// <summary>
    /// Creates a card number range from a string representation.
    /// The range separator can be escaped by prefixing it with the "\" character.
    /// </summary>
    public static CardNumberRange Parse(string cardNumberRangeString, string rangeSeparator)
    {
        var cardNumberRangeParts = cardNumberRangeString.SplitEscaped(
            rangeSeparator,
            StringSplitOptions.RemoveEmptyEntries | StringSplitOptions.TrimEntries
        );

        return cardNumberRangeParts.Count switch
        {
            1 => new CardNumberRange(cardNumberRangeParts[0]),
            2 => new CardNumberRange(cardNumberRangeParts[0], cardNumberRangeParts[1]),
            _ => throw new FormatException(
                $"Card number range string \"{cardNumberRangeString}\" isn't in the correct format."
            ),
        };
    }

    public static bool IsExpandable(string firstCardNumber, string lastCardNumber) => IsExpandable(
        new CardNumberInfo(firstCardNumber),
        new CardNumberInfo(lastCardNumber)
    );

    public static bool IsExpandable(CardNumberInfo firstCardNumberInfo, CardNumberInfo lastCardNumberInfo) =>
        firstCardNumberInfo.LeftPart.IntValue != null &&
        lastCardNumberInfo.LeftPart.IntValue != null;

    public IList<string> Expand()
    {
        if (this.lazyCardNumbers != null)
        {
            return this.lazyCardNumbers;
        }

        this.lazyCardNumbers = [];

        if (this.AreCardNumbersEqual)
        {
            this.lazyCardNumbers.Add(this.FirstCardNumber);

            return this.lazyCardNumbers;
        }

        var uniqueCardNumbers = new HashSet<string>();
        var firstNumber = this.FirstCardNumberInfo.LeftPart.IntValue!.Value;
        var lastNumber = this.LastCardNumberInfo.LeftPart.IntValue!.Value;

        var lastNumberStringLength =
            lastNumber.ToString().Length +
            this.LastCardNumberInfo.LeftPart.Trailing0Count;

        for (var number = firstNumber; number <= lastNumber; number++)
        {
            var numberString = number.ToString();

            var cardNumber =
                this.FirstCardNumberInfo.LeftPart.Prefix +
                ((this.FirstCardNumberInfo.LeftPart.Trailing0Count != 0)
                    ? numberString.PadLeft(lastNumberStringLength, '0')
                    : numberString
                ) +
                this.FirstCardNumberInfo.LeftPart.Suffix +
                ((this.FirstCardNumberInfo.RightPart != null)
                    ? $"/{this.FirstCardNumberInfo.RightPart.Value}"
                    : string.Empty
                );

            #pragma warning disable CA1868
            if (!uniqueCardNumbers.Contains(cardNumber))
            {
                uniqueCardNumbers.Add(cardNumber);
                this.lazyCardNumbers.Add(cardNumber);
            }
            #pragma warning restore CA1868
        }

        return this.lazyCardNumbers;
    }

    [return: NotNullIfNotNull(nameof(cardNumberRanges))]
    public static IList<string>? ExpandAll(IEnumerable<CardNumberRange>? cardNumberRanges)
    {
        if (cardNumberRanges == null)
        {
            return null;
        }

        var cardNumbers = new List<string>();
        var uniqueCardNumbers = new HashSet<string>();

        foreach (var cardNumberRange in cardNumberRanges)
        {
            foreach (var cardNumber in cardNumberRange.Expand())
            {
                if (!uniqueCardNumbers.Contains(cardNumber))
                {
                    cardNumbers.Add(cardNumber);
                    uniqueCardNumbers.Add(cardNumber);
                }
            }
        }

        return cardNumbers;
    }

    public override string ToString() => this.ToString(DefaultRangeSeparator);

    public string ToString(char rangeSeparator) => this.ToString(rangeSeparator.ToString());

    public string ToString(string rangeSeparator)
    {
        var escapedRangeSeparator = $@"\{rangeSeparator}";
        var escapedFirstCardNumber = this.FirstCardNumber.Replace(rangeSeparator, escapedRangeSeparator);

        return this.AreCardNumbersEqual
            ? escapedFirstCardNumber
            : escapedFirstCardNumber
                + rangeSeparator
                + this.LastCardNumber.Replace(rangeSeparator, escapedRangeSeparator);
    }
}
