using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace TcgCollector.PokemonTcg;

[JsonConverter(typeof(StringEnumConverter))]
public enum RegulationMark
{
    A,
    B,
    C,
    D,
    E,
    F,
    G,
    H,
    I,
    J,
    K,
}
