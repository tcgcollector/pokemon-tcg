# Pokémon TCG

A C# library for the Pokémon TCG.

## Contributing

See <https://github.com/egonolieux/personal-conventions>.

## Legal

See [LICENSE](LICENSE).
